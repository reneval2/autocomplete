import React, { useState } from 'react'
import './App.css'
import Autocomplete from './components/Autocomplete/Autocomplete'

function App() {
  const [value, setValue] = useState('')
  const [colors, setColors] = useState({})

  const getSuggestions = val => {
    if (!val) {
      return []
    }
    return fetch('/colors.json')
      .then(response => response.json())
      .then(data => {
        setColors(data.colors)
        return Object.keys(data.colors)
      })
      .then(
        colors => {
          const regex = new RegExp(`${val}`, 'gi')
          return colors.filter(color => color.search(regex) > -1)
        }
      )
  }

  return (
    <div className="App">
      <div className="result" style={{ background: colors[value] || 'white' }}><p>Current value is:</p> <b>{value}</b>
      </div>
      <div className="container">
        <p className="label">Start typing any CSS color name:</p>
        <Autocomplete
          value={value}
          onChange={val => {
            setValue(val)
          }}
          getSuggestions={getSuggestions}
        />
      </div>
      <hr />

    </div>
  )
}

export default App
