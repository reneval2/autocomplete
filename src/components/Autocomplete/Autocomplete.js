import React from 'react'
import PropTypes from 'prop-types'
import './Autocomplete.css'
import AutocompleteSuggestions from './AutocompleteSuggestions'
import { debounce } from './utiils'
import { DEBOUNCE_TIMEOUT, KEY_MAP, MAX_SUGGESTIONS } from './constats'


const showSuggestions = (suggestions, value) => {
  if (suggestions.length < 1 ||
    (suggestions.length === 1 && suggestions[0] === value)) {
    return false
  }
  return true
}

class Autocomplete extends React.Component {

  state = {
    currentValue: this.props.value,
    suggestions: [],
    selectedSuggestionIndex: 1,
    isLoadingSuggestion: false,
    showSuggestions: false
  }

  updateSuggestions = debounce(async value => {
    const { getSuggestions } = this.props
    try {
      let suggestions = await getSuggestions(value)
      suggestions = suggestions.slice(0, MAX_SUGGESTIONS)
      this.setState(() => ({
        suggestions,
        showSuggestions: showSuggestions(suggestions, value)
      }))
    } catch (e) {
      this.setState(() => ({
        suggestions: [],
        showSuggestions: false
      }))
    }
  }, DEBOUNCE_TIMEOUT)

  onKeyDown = event => {
    const { selectedSuggestionIndex, suggestions } = this.state
    const suggestionsLength = suggestions.length

    if (!suggestionsLength) {
      return
    }

    switch (event.keyCode) {
      case KEY_MAP.ENTER: {
        this.onSelectSuggestion(selectedSuggestionIndex)
        return
      }
      case KEY_MAP.ARROW_UP: {
        event.preventDefault()
        // go to the last if reach the the first
        const position = (suggestionsLength + selectedSuggestionIndex - 1) % suggestionsLength
        this.setActiveSuggestion(position)
        return
      }
      case KEY_MAP.ARROW_DOWN: {
        // go to first if reach the end
        const position = (selectedSuggestionIndex + 1) % suggestionsLength
        this.setActiveSuggestion(position)
        return
      }
      case KEY_MAP.ESC: {
        this.hideSuggestions()
        return
      }
    }
  }

  hideSuggestions = () => {
    this.setState({ showSuggestions: false })
  }

  onInputChange = e => {
    const { value } = e.target
    this.setState(() => ({ currentValue: value }))
    if (this.props.onChange) {
      this.props.onChange(value)
    }
    this.updateSuggestions(value)
  }

  setActiveSuggestion = index => {
    const { selectedSuggestionIndex } = this.state
    if (index !== selectedSuggestionIndex) {
      this.setState({
        selectedSuggestionIndex: index,
        showSuggestions: true
      })
    }
  }

  onSelectSuggestion = index => {
    const { suggestions } = this.state
    this.setState(
      ({ suggestions }) => ({
        selectedSuggestionIndex: -1,
        showSuggestions: false,
        currentValue: suggestions[index]
      }),
      this.props.onChange && this.props.onChange(suggestions[index])
    )
  }

  render() {
    const {
      suggestions,
      selectedSuggestionIndex,
      currentValue,
      showSuggestions
    } = this.state

    return (
      <div className="st-autocomplete">
        <input
          className="st-autocomplete-input"
          value={currentValue}
          onChange={this.onInputChange}
          onKeyDown={this.onKeyDown}
          onFocus={() => this.updateSuggestions(currentValue)}

        />
        {showSuggestions && (
          <AutocompleteSuggestions
            suggestions={suggestions}
            selectedSuggestionIdx={selectedSuggestionIndex}
            onSuggestionHover={this.setActiveSuggestion}
            onSuggestionClicked={this.onSelectSuggestion}
            substring={currentValue}
          />
        )}
      </div>
    )
  }
}

Autocomplete.propTypes = {
  value: PropTypes.string,
  getSuggestions: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
}

Autocomplete.defaultProps = {
  value: '',
}
export default Autocomplete
