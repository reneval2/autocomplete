
export const KEY_MAP = {
  ENTER: 13,
  ARROW_UP: 38,
  ARROW_DOWN: 40,
  ESC: 27
}

export  const DEBOUNCE_TIMEOUT = 500
export const MAX_SUGGESTIONS = 10
