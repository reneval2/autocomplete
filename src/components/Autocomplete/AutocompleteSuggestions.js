import React from 'react'
import './Autocomplete.css'

const itemClassNames = (idx, selectedSuggestionIdx) => {
  return `st-suggestion ${selectedSuggestionIdx === idx ? 'active' : ''}`
}
const getItemHtml = (string, substring) => {
  if (!substring || !string) {
    return string
  }
  const regex = new RegExp(`${substring}`, 'gi')
  return string.replace(regex, (str) => `<b>${str}</b>`)
}


function AutocompleteSuggestions({
  suggestions,
  substring,
  selectedSuggestionIdx,
  onSuggestionHover,
  onSuggestionClicked
}) {

  return (
    <ul
      className="st-suggestion-list"

    >
      {suggestions.map((suggestion, idx) => (
        <li
          className={itemClassNames(selectedSuggestionIdx, idx)}
          key={suggestion}
          onClick={() => onSuggestionClicked(idx)}
          onMouseOver={()=> onSuggestionHover(idx)}
          dangerouslySetInnerHTML={{
            __html: getItemHtml(suggestion, substring, idx)
          }}
        />
      ))}
    </ul>
  )
}

export default AutocompleteSuggestions
